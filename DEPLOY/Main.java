import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class Main {

	public static void main(String[] args) throws IOException {
		// machines.txt contient les hostnames des machines de la salle de TP
		FileReader f = new FileReader("/cal/homes/sbayon/2A/SLR/slr207/DEPLOY/machines.txt");
		BufferedReader b = new BufferedReader(f);
		String line;

		// processes pour la création des répertoires pour la copie de slave.jar
		ArrayList<ProcessBuilder> processBuilders = new ArrayList<ProcessBuilder>();
		ArrayList<ProcessBuilder> scpProcesses = new ArrayList<ProcessBuilder>();
		
		// process de test de la connection SSH et affichage de l'hostname de la machine
		Process p;
		ProcessBuilder pb1;
		boolean success = false; //connection ssh réussie
		
		while((line = b.readLine()) != null)
		{
			success = false;
			processBuilders = new ArrayList<ProcessBuilder>();
			scpProcesses = new ArrayList<ProcessBuilder>();
			pb1 = new ProcessBuilder("ssh", "sbayon@" + line, "hostname");
			p = pb1.start();

			// délai pour la connection SSH
			try {
				success = p.waitFor(1, TimeUnit.SECONDS);
			} catch (InterruptedException e) {e.printStackTrace();}
			
			// si la connection ssh a réussi, création des répertoires et copie de slave.jar
			if(success)
			{
				System.out.println("connected to " + line);
				processBuilders.add(new ProcessBuilder("ssh", "sbayon@"+line, "mkdir -p /tmp/sbayon/maps/"));
				processBuilders.add(new ProcessBuilder("ssh", "sbayon@"+line, "mkdir -p /tmp/sbayon/splits/"));
				processBuilders.add(new ProcessBuilder("ssh", "sbayon@"+line, "mkdir -p /tmp/sbayon/reduces/"));
				scpProcesses.add(new ProcessBuilder("scp", "/tmp/sbayon/SLAVE.jar", "sbayon@"+line+":/tmp/sbayon/SLAVE.jar"));
			}
			else
				System.out.println("could not connect to " + line);

			// traitement des erreurs
			for (ProcessBuilder pb : processBuilders)
			{
				pb.redirectErrorStream(true);
				pb.inheritIO();
				try {
					p = pb.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			for (ProcessBuilder pb : processBuilders)
			{
				pb.redirectErrorStream(true);
				pb.inheritIO();
				try {
					p.waitFor(5, TimeUnit.SECONDS);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			for (ProcessBuilder pb : scpProcesses)
			{
				pb.redirectErrorStream(true);
				pb.inheritIO();
				try {
					p = pb.start();
					p.waitFor(5, TimeUnit.SECONDS);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		b.close();
		f.close();
	}

}
