import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;


public class Main {

	public static void main(String[] args) throws IOException {
		FileReader f = new FileReader("/cal/homes/sbayon/2A/SLR/slr207/DEPLOY/machines.txt");
		BufferedReader b = new BufferedReader(f);
		BufferedReader processBR;
		ArrayList<Integer> tempList;
		String line;
		ProcessBuilder pb1, pb2;
		String key;
		HashMap<Process, Integer> processes = new HashMap<Process, Integer>();
		HashMap<Integer, String> machineMap = new HashMap<Integer, String>();
		HashMap<Integer, String> reduceMachineMap = new HashMap<Integer, String>();
		HashMap<String, ArrayList<Integer>> keyMap = new HashMap<String, ArrayList<Integer>>();
		HashMap<String, ArrayList<Integer>> fileCopyMap = new HashMap<String, ArrayList<Integer>>();
		ArrayList<ProcessBuilder> reducePB = new ArrayList<ProcessBuilder>();
		ArrayList<ProcessBuilder> shufflePB = new ArrayList<ProcessBuilder>();
		ArrayList<Process> shuffleProc = new ArrayList<Process>();
		ArrayList<Process> reduceProc = new ArrayList<Process>();
		ArrayList<Integer> keyFiles = new ArrayList<Integer>();

		int i = 0;

		// test de la connection SSH
		boolean success = false; // vrai ss une connection SSH a été réussie
		ProcessBuilder pb;
		Process p;
		String temp;
		int splitNb = Integer.parseInt(args[0]);

		while(i < splitNb)
		{
			do{
				if((line=b.readLine()) == null)
				{
					b = new BufferedReader(f);
					b.readLine();
				}


				pb = new ProcessBuilder("ssh", "sbayon@" + line, "hostname");
				p = pb.start();
				try {
					success = p.waitFor(500, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {e.printStackTrace();}
			}while(!success);

			// Une connexion SSH a été réussie
			System.out.println("SSH connection : connected to " + line);

			// Copie et exécution de SLAVE sur la machine où l'on est connectés
			pb1 = new ProcessBuilder("scp", "/tmp/sbayon/splits/S"+Integer.toString(i)+".txt", "sbayon@"+line+":/tmp/sbayon/splits/S"+Integer.toString(i)+".txt");
			pb2 = new ProcessBuilder("ssh", "sbayon@"+line, "java", "-jar", "/tmp/sbayon/SLAVE.jar", "0", "/tmp/sbayon/splits/S"+Integer.toString(i)+".txt");

			// Phase de mapping
			machineMap.put(i, line);

			try {
				pb2.redirectErrorStream(true);
				pb1.start();
				processes.put(pb2.start(), i);
			} catch (Exception e) {e.printStackTrace();}
			i++;
		}
		System.out.println();

		for(Entry<Process, Integer> entry: processes.entrySet())
		{
			try {
				entry.getKey().waitFor();
			} catch (InterruptedException e) {e.printStackTrace();}

			// Flux de la sortie standard du SLAVE pour l'affichage des clés
			processBR = new BufferedReader(new InputStreamReader(entry.getKey().getInputStream()));

			while((key = processBR.readLine()) != null)
			{
				if(keyMap.containsKey(key))
					tempList = keyMap.get(key);
				else
					tempList = new ArrayList<Integer>();
				tempList.add(entry.getValue());
				keyMap.put(key, tempList);
			}

		}

		// affichage du dictionnaire UMx - machines
		System.out.println("Lancement des SLAVE par le master (pour la phase map)");
		for(Entry<Integer, String> entry : machineMap.entrySet())
			System.out.println("UM"+entry.getKey() + " - " + entry.getValue());
		System.out.println();

		//affichage du dictionnaire clés - listes des UMx
		System.out.println("Affichage du dictionnaire clés - listes des UMx");
		for(Entry<String, ArrayList<Integer>> entry : keyMap.entrySet())
		{
			System.out.print("clé : "+ entry.getKey()+" - liste d'UMs : " );
			for(int j : entry.getValue())
				System.out.print("UM"+Integer.toString(j)+" ");
			System.out.println();
		}
		System.out.println("Mapping phase ended");
		System.out.println();
		b.close();

		// Préparation de la phase de shuffle
		f = new FileReader("/cal/homes/sbayon/2A/SLR/slr207/DEPLOY/machines.txt");
		b = new BufferedReader(f);
		i = 0;
		for(Entry<String, ArrayList<Integer>> entry : keyMap.entrySet())
		{
			// connection SSH
			do{
				if((temp = b.readLine())== null)
				{
					b.close();
					f = new FileReader("/cal/homes/sbayon/2A/slr207/DEPLOY/machines.txt");
					b = new BufferedReader(f);
					temp = b.readLine();
				}
				pb = new ProcessBuilder("ssh", "sbayon@" + temp, "hostname");
				p = pb.start();
				try {
					success = p.waitFor(500, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {e.printStackTrace();}
			}while(!success);

			String toProcess = "";
			keyFiles = entry.getValue();

			// Recopie des UMx manquantes
			ArrayList<Integer> filesOnMachine;
			for(int file : keyFiles)
			{
				if((filesOnMachine = fileCopyMap.get(temp)) != null && filesOnMachine.contains(file))
					continue;
				if(filesOnMachine == null)
					filesOnMachine = new ArrayList<Integer>();
				filesOnMachine.add(file);
				fileCopyMap.put(temp, filesOnMachine);
				pb1 = new ProcessBuilder("scp", "sbayon@"+machineMap.get(file)+":/tmp/sbayon/maps/UM"+file+".txt", "sbayon@"+temp+":/tmp/sbayon/maps/UM"+file+".txt"); 
				shuffleProc.add(pb1.start());
				toProcess += "/tmp/sbayon/maps/UM"+file+".txt ";
			}
			for(Process pTemp : shuffleProc)
			{
				try{
					pTemp.waitFor();
				}catch(InterruptedException e){e.printStackTrace();}
			}
			System.out.println(toProcess);

			// Lancement de la phase Shuffle	
			shufflePB.add(new ProcessBuilder("ssh", "sbayon@"+temp, "java", "-jar", "/tmp/sbayon/SLAVE.jar", "1", entry.getKey(), "/tmp/sbayon/maps/SM"+i+".txt", toProcess));

			// Lancement du reduce
			reducePB.add(new ProcessBuilder("ssh", "sbayon@"+temp, "java", "-jar", "/tmp/sbayon/SLAVE.jar", "2", entry.getKey(), "/tmp/sbayon/maps/SM"+i+".txt", "/tmp/sbayon/reduces/RM"+i+".txt"));

			reduceMachineMap.put(i, temp);
			i++;
		}	
		
		long shuffle_start_time = System.currentTimeMillis();
		
		for(ProcessBuilder pB: shufflePB)
		{
			shuffleProc.add(pB.start());
		}
		// attend que la phase de shuffle soit terminée
		for(Process pTemp: shuffleProc)
		{
			try {pTemp.waitFor();} catch (InterruptedException e) {e.printStackTrace();}
		}
		
		long shuffle_end_time = System.currentTimeMillis();
		long shuffle_time = shuffle_end_time-shuffle_start_time;

		System.out.println("Shuffle phase ended");
		System.out.println("Time elapsed : " + shuffle_time + "ms");
		System.out.println();
		
		long reduce_start_time = System.currentTimeMillis();
		
		// Phase de shuffle terminée - lancement phase de reduce
		for(ProcessBuilder pbTemp : reducePB)
			reduceProc.add(pbTemp.start());
		for(Process pTemp : reduceProc)
			try {
				pTemp.waitFor();
			} catch (InterruptedException e) {e.printStackTrace();} 
		
		long reduce_end_time = System.currentTimeMillis();
		long reduce_time = reduce_end_time-reduce_start_time;

		// Fin de la phase de Reduce
		System.out.println("Reducing phase ended");
		System.out.println("Time elapsed : " + reduce_time + "ms");
		System.out.println();
		System.out.println("Affichage du dictionnaire RMx - machine");

		// affichage du dictionnaire RMx - machines
		for(Entry<Integer, String> entry : reduceMachineMap.entrySet())
			System.out.println("RM"+entry.getKey()+" " + entry.getValue());

		System.out.println();
		System.out.println("Résultat final : ");
		for(Entry<Integer, String> entry : reduceMachineMap.entrySet())
		{
			
			pb1 = new ProcessBuilder("scp", "sbayon@"+entry.getValue()+":/tmp/sbayon/reduces/RM"+entry.getKey()+".txt", "/tmp/sbayon/reduces/RM"+entry.getKey()+".txt");
			//pb1.inheritIO();
			p = pb1.start();
			try {
				p.waitFor();
			} catch (InterruptedException e) {e.printStackTrace();}	
			try{
				f = new FileReader("/tmp/sbayon/reduces/RM"+entry.getKey()+".txt");
				b = new BufferedReader(f);
				System.out.println(b.readLine());
			}catch(IOException e){e.printStackTrace();}
		}
		b.close();
	}
}
