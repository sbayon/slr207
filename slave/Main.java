import java.io.*;
import java.util.*;

public class Main {
	public static void main(String[] args){
		// argument 1 : mode de fonctionnement
		String mode =args[0];

		// Calcul du map
		if (mode.equals("0")){
			String split_name = args[1];
			char x = split_name.charAt(split_name.lastIndexOf('.')-1);
			String map_name = "/tmp/sbayon/maps/UM" + x + ".txt";
			try {
				count(split_name, map_name);
			} catch (Exception e) {}
		}

		// Shuffle
		if (mode.equals("1")){
			String key = args[1];
			String SM_name = args[2];
			ArrayList<String> UMx = new ArrayList();
			for (int i=3; i<args.length; i++){
				UMx.add(args[i]);
			}
			try {
				shuffle(SM_name, UMx, key);
			} catch (Exception e) {}
		}

		// Reduce
		if (mode.equals("2")){
			String key = args[1];
			String SM_name = args[2];
			String RM_name = args[3];

			try {
				reduce(RM_name, SM_name, key);
			} catch (Exception e) {}
		}

	}

	// Comptage pour la phase de Map
	public static void count(String input, String output){
		HashMap<String, Integer> map = new HashMap<String, Integer>();		
		try {
			BufferedReader in = new BufferedReader(new FileReader(input));
			PrintWriter w = new PrintWriter(output);
			String line;
			StringTokenizer st;
			String word;
			int nbOcc;

			//fichier pour les résultats

			while ((line = in.readLine()) != null)
			{
				st = new StringTokenizer(line, " ,.;:_-+*/\\.;\n\"'{}()=><\t!?");
				while(st.hasMoreTokens())
				{
					word = st.nextToken();
					w.println(word + " " + "1");
					map.put(word,1);   
				}
			}


			Set<String> allWords = map.keySet();

			for (String current: allWords){
				System.out.println(current);
			}
			w.close();

		} catch (Exception e) {}
	}

	// Pour trier la HashMap
	public static HashMap <String, Integer> sort(HashMap <String, Integer> map){
		List<Map.Entry <String,Integer >> list = new LinkedList<Map.Entry<String, Integer>>( map.entrySet());

		Collections.sort( list, new Comparator<Map.Entry<String, Integer>>(){
			public int compare(Map.Entry<String,Integer> o1, Map.Entry<String,Integer> o2){
				int result = (o2.getValue()).compareTo( o1.getValue());
				if (result == 0)
					result = (o1.getKey()).compareTo(o2.getKey());
				return result;
			}
		});

		HashMap<String, Integer> map_sorted = new LinkedHashMap<String, Integer>();
		for(Map.Entry<String, Integer> entry : list)
			map_sorted.put(entry.getKey(), entry.getValue());
		return map_sorted;
	}

	// Shuffle
	public static void shuffle (String SM_name, ArrayList<String> UMx, String key){
		try {
			PrintWriter w = new PrintWriter(SM_name);
			for (String UM : UMx){
				BufferedReader um = new BufferedReader(new FileReader(UM));
				String line;
				int len = key.length();
				String first_word;

				while ((line = um.readLine()) != null)
				{
					first_word = line.substring(0,len);
					System.out.println(first_word);
					if (first_word.equals(key)){
						w.println(line);
					}
				}	 
			}
			w.close();
		} catch (Exception e) {}
	}

	// Reduce
	public static void reduce(String RM_name, String SM_name, String key) {
		try {
			BufferedReader sm = new BufferedReader(new FileReader(SM_name));
			PrintWriter w = new PrintWriter(RM_name);
			String line;
			int nbOcc = 0;
			while ((line = sm.readLine()) != null){
				nbOcc++;
			}
			w.println(key + " " + nbOcc);
			w.close();
		} catch (Exception e) {}
	}	
}
