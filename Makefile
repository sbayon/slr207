JC = javac
JVM = java
.SUFFIXES: .java .class

.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	  MASTER/Main.java \
	  slave/Main.java \
	  DEPLOY/Main.java \
	  

default: classes

classes: $(CLASSES:.java=.class)

slave_jar:
	cd slave && jar cvmf META-INF/MANIFEST.MF ../SLAVE.jar Main.class Main\$1.class

deploy:
	cp SLAVE.jar /tmp/sbayon && cd DEPLOY && $(JVM) Main

slave_test:
	cd slave && java Main $(ARGS)

slave_jar_test:
	java -jar SLAVE.jar $(ARGS)

master:
	rm -rf /tmp/sbayon && mkdir /tmp/sbayon && cp -r ./tmp/sbayon /tmp/ && make deploy && cd MASTER && java Main $(ARGS)
tmp:
	cp -r ./tmp /tmp
clean:
	$(RM) MASTER/*.class slave/*.class DEPLOY/*.class

.PHONY: clean tmp

