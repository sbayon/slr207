import java.io.*;
import java.util.*;

public class Counter {
	public static void main(String[] argv) throws Exception
	{

		long startTime = System.currentTimeMillis();
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();		
		BufferedReader in = new BufferedReader(new FileReader(argv[0]));
		String line;
		StringTokenizer st;
		String word;
		int nbOcc;
                
		//fichier pour les résultats
		PrintWriter w = new PrintWriter(argv[1]);

		while ((line = in.readLine()) != null)
		{
			st = new StringTokenizer(line, " ,.;:_-+*/\\.;\n\"'{}()=><\t!?");
			while(st.hasMoreTokens())
			{
				word = st.nextToken();
				if (map.containsKey(word))
				{
					nbOcc = ((Integer)map.get(word)).intValue();
					nbOcc++;
				}
				else nbOcc = 1;
				map.put(word, new Integer(nbOcc));
			}
		}

		long endOccTime = System.currentTimeMillis();
		
		map = sort(map);
		
		long endSortTime = System.currentTimeMillis();
		
		Set<String> allWords = map.keySet();

		for (String current: allWords){
			nbOcc = ((Integer)map.get(current)).intValue();
			System.out.println(current + ":" + nbOcc);
			w.println(current + " " + nbOcc);
		}
                w.close();
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime ;
		long occTime = endOccTime - startTime ;
		long sortTime = endSortTime - endOccTime ; 

		System.out.println( "Temps total : " + totalTime + " ms");	          System.out.println( "Temps pour compter le nombre d'occurences : " +  occTime + " ms");
		System.out.println( "Temps pour trier : " + sortTime + " ms");


	}
	// Pour trier la HashMap
	public static HashMap <String, Integer> sort(HashMap <String, Integer> map){
		List<Map.Entry <String,Integer >> list = new LinkedList<Map.Entry<String, Integer>>( map.entrySet());

		Collections.sort( list, new Comparator<Map.Entry<String, Integer>>(){
			public int compare(Map.Entry<String,Integer> o1, Map.Entry<String,Integer> o2){
				int result = (o2.getValue()).compareTo( o1.getValue());
				if (result == 0)
					result = (o1.getKey()).compareTo(o2.getKey());
				return result;
			}
		});

		HashMap<String, Integer> map_sorted = new LinkedHashMap<String, Integer>();
		for(Map.Entry<String, Integer> entry : list)
			map_sorted.put(entry.getKey(), entry.getValue());
		return map_sorted;
	}




}
